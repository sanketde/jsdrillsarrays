function find(elements, cb) {

    let arr = [];
    if(elements === undefined || !Array.isArray(elements) ||elements.length === 0)
    {
        return arr;
    }
    for (let index = 0; index < elements.length; index++) {
        if (cb(elements[index], index, elements)) {
            return elements[index];
        }
    }
}
module.exports = find