function each(elements, cb) {
    let arr = [];
    if(elements === undefined || !Array.isArray(elements) || elements.length === 0)
    {
        return arr;
    }
    for (let index = 0; index < elements.length; index++) {
        cb(elements[index], index, elements);
    }
}
module.exports = each