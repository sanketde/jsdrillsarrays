function reduce(elements, cb, acc) {

    let arr = [];
    if(elements === undefined || !Array.isArray(elements) ||elements.length === 0)
    {
        return arr;
    }
    // let acc = start;
    for (let index = 0; index < elements.length; index++) {
        if(acc===undefined){
            acc=elements[index]
            continue;
        }
        acc = cb(acc, elements[index], index, elements);
    }
    return acc;
}

module.exports = reduce;