let find = require('../find.cjs')
const items = [1, 2, 3, 4, 5, 5];

let result = find(items, (element) => {
    return element > 2;
});

console.log(result);