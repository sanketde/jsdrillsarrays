let reduce = require('../reduce.cjs');

const items = [1, 2, 3, 4, 5, 5];

let result = reduce(items, (acc, element) => {
    return acc + element;
}, 0);

console.log(result);